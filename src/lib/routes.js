import path from 'path';
import Home from './controllers/home';
import * as Note from './controllers/note';

export default [
  {
    method: 'GET',
    path: '/',
    handler: Home,
    config: {
      description: 'Gets all the notes available'
    }
  },
  {
    method: 'POST',
    path: '/note',
    handler: Note.create,
    config: {
      description: 'Adds a new note'
    }
  },
  {
    method: 'GET',
    path: '/note/{slug}',
    handler: Note.read,
    config: {
      description: 'Gets the content of a note'
    }
  },
  {
    method: 'PUT',
    path: '/note/{slug}',
    handler: Note.update,
    config: {
      description: 'Updates the selected note'
    }
  },
  {
    method: 'DELETE',
    path: '/note/{slug}',
    handler: Note.deleteNote,
    config: {
      description: 'Deletes the selected note'
    }
  }
];
