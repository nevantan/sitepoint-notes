import Models from '../models';

export default async (request, h) => {
  const notes = await Models.Note.findAll({
    order: [['date', 'DESC']]
  });

  return { notes };
};
