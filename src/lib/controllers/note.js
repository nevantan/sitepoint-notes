import path from 'path';
import slugify from 'slugify';
import Models from '../models';

export const create = async (request, h) => {
  const note = await Models.Note.create({
    date: new Date(),
    title: request.payload.noteTitle,
    slug: slugify(request.payload.noteTitle, {
      lower: true
    }),
    description: request.payload.noteDescription,
    content: request.payload.noteContent
  });
  return { note };
};

export const read = async (request, h) => {
  const note = await Models.Note.findOne({
    where: { slug: request.params.slug }
  });

  return { note };
};

export const update = async (request, h) => {
  const values = {
    title: request.payload.noteTitle,
    description: request.payload.noteDescription,
    content: request.payload.noteContent
  };

  const options = {
    where: { slug: request.params.slug }
  };

  await Models.Note.update(values, options);
  const note = await Models.Note.findOne(options);

  return { note };
};

export const deleteNote = async (request, h) => {
  await Models.Note.destroy({
    where: {
      slug: request.params.slug
    }
  });

  return h.redirect('/');
};
