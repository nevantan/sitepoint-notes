import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import Settings from '../../settings';

const dbSettings = Settings[process.env.NODE_ENV].db;
const sequelize = new Sequelize(
  dbSettings.database,
  dbSettings.user,
  dbSettings.password,
  dbSettings
);

const db = {};

fs.readdirSync(path.join(__dirname, '..', 'src', 'lib', 'models'))
  .filter((file) => file.indexOf('.') !== 0 && file !== 'index.js')
  .forEach((file) => {
    const model = sequelize.import(
      path.join(__dirname, '..', 'src', 'lib', 'models', file)
    );
    db[model.name] = model;
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
