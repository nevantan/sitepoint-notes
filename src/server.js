import Models from './lib/models';

import dotenv from 'dotenv';
dotenv.config({
  path: '.env'
});

import { Server } from 'hapi';
const server = new Server({ port: process.env.PORT });

import Routes from './lib/routes';

server.route(Routes);

const init = async () => {
  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  console.error(err);
  process.exit(1);
});

Models.sequelize.sync().then(() => {
  init();
});
